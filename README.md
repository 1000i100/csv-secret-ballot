EN | [FR](README.fr.md)

# CSV-secret-ballot
**Make online secret balloting easy.**

TODO: translate what's next

- If you are used to using online surveying tools, this added feature will allow you to do online secret balloting with your favorite survey tool.
- If you are not used to using online surveying tools, we recommend that you try out [JugementMajoritaire](https://jugementmajoritaire.net/)
(or else learn how to use the surveying tool [FramaDate](https://framadate.org) in combination with CSV-secret-ballot).


## User's guide

In order to conceal each person's vote from the other participants, and to verify that nobody cheats, there needs to be at least two teams:
- One **Preparation Team**, who knows to whom belongs each token, in order to distribute them to each participant.
- One **Counting Team**, who knows which token has voted what, but who does not know to whom each token belongs.

The **Agenda Team**, which can be either the same as **Counting Team** or a third one, concieves the survey with the different voting options.
It's job is not related to secrecy keeping.

In order to prevent cheats, it is recommended to have teams of several people in charge of each role instead of individuals.


### Agenda Team :
1. You can use any online survey/form tool as long as the results can be exported in .csv format with a vote per line.

   For exemple : [FramaDate](https://framadate.org), [FramaForms](https://framaforms.org/), [LimeSurvey](https://www.limesurvey.org/), [OhMyForm](https://ohmyform.com/), or for WordPress [Contact Form 7](https://fr.wordpress.org/plugins/contact-form-7/) + [CFDB7](https://fr.wordpress.org/plugins/contact-form-cfdb7/)

2. Include in your survey/form tool a box in which each participant will enter his/her token. If votes by proxy are used, they can either be entered in the same box as the others 
or have a second box dedicated to them.
3. Share the public link to the poll to the **Preparation Team**
4. Share the admin link to the poll to the **Counting Team**

### Preparation Team :
1. Use a spreadsheet in which to list the participants to the vote (with their names, e-mail, voting in person or by proxy...
whatever you like as long as it's organized with one voter per line)
2. Export the list in .csv with tabulations as seperator (.tsv) 
3. Import it in [csv-secret-ballot-generate](https://1000i100.frama.io/csv-secret-ballot/generate.html)
4. It will generate 2 files : `valid-token-list.txt` to send to the **Counting Team**, and `token-identity-list.csv`.
5. Open the file `token-identity-list.csv`in a spreadsheet. You will find the content of the original file preceded by a unique token per voter.
6. Transfer the tokens to each voter by the mean of your choice (ideally by a secured chanel). If there are votes by proxy, transfer the tokens of the voters by proxy to the person voting for them).

### Counting Team :
1. Use the admin link to the poll to export the results of the vote in .csv with tabulations as separators (.tsv). There should be one vote per line.
2. Import in [csv-secret-ballot-verify](https://1000i100.frama.io/csv-secret-ballot/verify.html) the results file you have just exported as well as the `valid-token-list.txt` file recieved from the Preparation Team.
3. It will generate a .zip file, which after being unzipped, will give you 5 files :
   - "abstentions.txt", which lists the tokens that have absteined themselves
   - "potentialErrors.txt", which lists potential errors with their explanations
   - "counted.txt", which shows the number of counted votes and lists the associated tokens
   - "RecapChart.csv", which shows the voting results for each question subject to the vote
   - "ValidResults.csv", which shows the voting results for each token
4. En bonus, vous avez le fichier de vote avec une réponse par ligne (après suppression des votes invalides et duplication des ligne avec votant + procurations pour arriver à 1 ligne = 1 voix.


## Alternatives

Other voting tools exist that do not need to trust teams of organisers.
The most secured system not only is hidden from the voting parties but also from the system's administrator (trustless polling) : [Belenios](http://www.belenios.org/)

However, in most cases, having data accessible to the system's administrator who is independent from the voting group, is not a real problem.
In French I recommend : [JugementMajoritaire](https://jugementmajoritaire.net/)

Other :
- [LiquidFeedback](https://liquidfeedback.org/) ([code source](https://www.public-software-group.org/liquid_feedback))
- [DemocracyOS, Decidim, Consul, VoteIT...](https://alternativeto.net/software/democracyos/?license=opensource)
- https://democracy.foundation/similar-projects/

