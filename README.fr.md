[EN](README.md) | FR

# CSV-secret-ballot
**Faites simplement du vote à bulletin secret.**

- Vous avez l'habitude d'un outil de sondage, de formulaire ? Vous pouvez l'utiliser pour du vote à bulletin secret !
- Vous ne connaissez aucun outil pour ça ? Regardez du coté de [JugementMajoritaire](https://jugementmajoritaire.net/)
  (ou apprenez à utiliser l'outil de sondage [FramaDate](https://framadate.org) en combinaison avec CSV-secret-ballot).


## Guide d'utilisation

Pour que personne ne puisse savoir qui vote quoi, et qu'il y ai du monde pour vérifier que personne ne triche, il faut au moins deux équipes :
- Une **équipe préparation** qui sait à qui chaque jeton appartient, pour pouvoir les leur transmettre.
- Une **équipe dépouillement**, qui peut savoir quel jeton à voté quoi, mais qui ne sais pas à qui appartient un jeton.

 Une **équipe ordre du jour**, qui peut être soit une des deux équipes précédentes, soit une troisième équipe,
 conçoit le formulaire avec les différents choix de votes. Elle ne manipule rien en rapport avec l'anonymat.

Je parle d'équipe plutôt que de personne seule pour que personne ne puisse ajouter en douce des jetons de vote valide, ou modifier le vote de quelqu'un sans que les autres ne le voient. 

### Equipe ordre du jour :
1. Vous pouvez utiliser n'importe quel outil de vote/sondage/questionnaire de votre choix du moment que les résultats puissent être exportés en csv avec un résultat/vote par ligne.

   Par exemple : [FramaDate](https://framadate.org), [FramaForms](https://framaforms.org/), [LimeSurvey](https://www.limesurvey.org/), [OhMyForm](https://ohmyform.com/), ou sur WordPress [Contact Form 7](https://fr.wordpress.org/plugins/contact-form-7/) + [CFDB7](https://fr.wordpress.org/plugins/contact-form-cfdb7/)

2. Incluez dans votre formulaire un champ ou chaque votant inscriera son code/jeton (et des jetons de procuration le cas échéant). En option, un 2nd champs dédié au procuration peut être utilisé.
3. Transmettez l'adresse du formulaire de vote à l'équipe préparation

### Equipe préparation :
1. dans un tableur, listez les votants (avec leur nom, leur email, votant, représenté/procuration... ce qui vous arrange mais 1 par ligne)
2. exportez la liste en .csv avec tabulation comme séparateur (.tsv) 
3. importez-la dans [csv-secret-ballot-generate](https://1000i100.frama.io/csv-secret-ballot/generate.html)
4. en plus de votre fichier d'origine vous voici avec 2 fichiers : `valid-token-list.txt`, à transmettre à l'équipe dépouillement, et `token-identity-list.csv` à utiliser au point suivant.
5. ouvrez dans un tableur `token-identity-list.csv` vous y trouverez le contenu de votre fichier initial précédé d'un jeton unique par votant.
6. transmettez par le moyen de votre choix (idéaleent un canal sécurisé) les jetons/code à chacun des votant (bonus, s'il y a des procuration, transmettez les jetons des procurrés à qui les représentera).

### Equipe dépouillement :
1. exportez les résultats du vote (quel que soit l'outil) en .csv avec tabulation comme séparateur (.tsv). Il devrait y avoir une réponse/vote par ligne (peut inclure des procurations)
2. importez dans [csv-secret-ballot-verify](https://1000i100.frama.io/csv-secret-ballot/verify.html) les résultat de vote que vous venez d'exporter en csv et `valid-token-list.txt`
3. vous voici avec en quelques lignes/fichiers : abstentions, exprimés, récap, erreurs, résultats ; le nombre de votant, s'il y a eu des erreurs (avec explications associées), et les résultats des votes.
4. En bonus, vous avez le fichier de vote avec une réponse par ligne (après suppression des votes invalides et duplication des ligne avec votant + procurations pour arriver à 1 ligne = 1 voix.


## Alternatives

Sans avoir des équipes organisatrices aux quelles il est nécessaire de faire confiance, il y a : [Belenios](http://www.belenios.org/)

Je recommande également, en français : [JugementMajoritaire](https://jugementmajoritaire.net/)


Autres :
- [LiquidFeedback](https://liquidfeedback.org/) ([code source](https://www.public-software-group.org/liquid_feedback))
- [DemocracyOS, Decidim, Consul, VoteIT...](https://alternativeto.net/software/democracyos/?license=opensource)
- https://democracy.foundation/similar-projects/

