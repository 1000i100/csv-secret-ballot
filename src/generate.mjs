
export default function generate(fileContent){
	const contentLines = splitByLine(fileContent);
	const tokenList = [];
	for(let i=0; i<contentLines.length; i++){
		const token = randomToken(9);
		tokenList.push(token);
		contentLines[i] = `${token}\t${contentLines[i]}`;
	}
	tokenList.sort();
	return {
		"valid-token-list.txt":tokenList.join('\r\n'),
		"token-identity-list.csv":contentLines.join('\r\n'),
	};
}
function splitByLine(str){
	return str.trim().split(/[\r\n]+/);
}
function randomToken(length) {
	const alphabet = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
	let token = []
	for(let i = 0; i<length;i++) token.push(alphabet[Math.floor(Math.random()*alphabet.length)]);
	return token.join('');
}
