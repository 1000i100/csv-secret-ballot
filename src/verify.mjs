
export default function verify(fileContent1,fileContent2){
	const validTokens = splitByLine(getFirstTokenList(fileContent1,fileContent2));
	const csvRawResult = splitByLine(getFirstNotTokenList(fileContent1,fileContent2));

	const warningList = [];
	const resultByToken = {};
	for(let line of csvRawResult){
		let unknow = true;
		for(let token of validTokens){
			if(line.includes(token)){
				if(resultByToken[token]) warningList.push(`Doublon pour ${token}, précédent résultat ignoré, dernier résultat pris en compte`);
				resultByToken[token] = line.replace(token,` **${token}** `);
				unknow = false;
			}
		}
		if(unknow) warningList.push(`Ligne ignorée : sans jeton d'authentification valide`);
	}
	const exprimés = Object.keys(resultByToken);
	const abstentions = validTokens.filter((token)=>!exprimés.includes(token));
	const validResultOneByLine = Object.values(resultByToken);
	let resultSum;
	try{
		const array2dVoteVoter = invertDimension(validResultOneByLine.map(line=>line.split('\t')));
		const sums = array2dVoteVoter.map(resList=>{
			const choices = {};
			for(let vote of resList){
				if(choices[vote]) choices[vote]++;
				else choices[vote]=1;
			}
			const arrayChoices = [];
			for(let c in choices){
				if(c[c.length-1]==='"') arrayChoices.push(`${c.substr(0,c.length-1)}:${choices[c]}"`);
				else arrayChoices.push(`${c}:${choices[c]}`);
			}
			return arrayChoices.sort();
		});
		resultSum = invertDimension(sums);
	} catch (e) {
		warningList.push('Les résultats ne sont pas séparés par des tabulations, calcul des sommes non pris en charge.');
	}
	const toReturn = {
		"exprimés.txt":`${exprimés.length}\r\n${exprimés.join('\r\n')}`,
		"abstentions.txt":`${abstentions.length}\r\n${abstentions.join('\r\n')}`,
		"erreursPotentielles.txt":`${warningList.length}\r\n${warningList.join('\r\n')}`,
		"tableauResultatsValides.csv":validResultOneByLine.join('\n'),
	};
	if(resultSum) toReturn["tableauRecapitulatif.csv"]=resultSum.map(r=>r.join('\t')).join('\r\n');
	return toReturn;
}
function isTokenList(fileContent) {
	let itIs = true;
	splitByLine(fileContent).forEach( (str)=>str.match(/^[a-zA-Z0-9]+$/)?'cool, do nothing':itIs=false );
	return itIs;
}
function getFirstTokenList(f1,f2) {
	if(isTokenList(f1)) return f1;
	if(isTokenList(f2)) return f2;
}
function getFirstNotTokenList(f1,f2) {
	if(!isTokenList(f1)) return f1;
	if(!isTokenList(f2)) return f2;
}
function splitByLine(str){
	return str.trim().split(/[\r\n]+/);
}
function invertDimension(tab) {
	const inverted = [];
	for(let i in tab){
		for(let j in tab[i]){
			if(typeof inverted[j] === "undefined") inverted[j] = [];
			inverted[j][i] = tab[i][j];
		}
	}
	return inverted;
}
