#!/usr/bin/env node
//const verify = require('../verify.js');
import generate from "../generate.mjs";
//const fs = require('fs');
import fs from "fs";

const programName = process.argv[1].split('/').pop();
let file1 = process.argv[2];
if(process.argv.length !== 3){
	console.error(`\nUsage : ${programName} voterIdentityList.csv`);
	process.exit(1);
}
const strFile1 = fs.readFileSync(file1,"utf8");

const generated = generate(strFile1);
Object.keys(generated).forEach((fileName)=>{
	const pathParts = fileName.split('/');
	pathParts.pop();
	if(pathParts.length>0) fs.mkdirSync(pathParts.join('/'),{recursive: true});
	fs.writeFileSync(fileName, outputfilesStrings[fileName])
});
