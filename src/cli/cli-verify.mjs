#!/usr/bin/env node
//const verify = require('../verify.js');
import verify from "../verify.mjs";
//const fs = require('fs');
import fs from "fs";

const programName = process.argv[1].split('/').pop();
let file1 = process.argv[2];
let file2 = process.argv[3];
if(process.argv.length !== 4){
	console.error(`\nUsage : ${programName} valid-token-list.txt resultFile.csv`);
	process.exit(1);
}
const strFile1 = fs.readFileSync(file1,"utf8");
const strFile2 = fs.readFileSync(file2,"utf8");

const verified = verify(strFile1,strFile2);
Object.keys(verified).forEach((fileName)=>{
	const pathParts = fileName.split('/');
	pathParts.pop();
	if(pathParts.length>0) fs.mkdirSync(pathParts.join('/'),{recursive: true});
	fs.writeFileSync(fileName, outputfilesStrings[fileName])
});
